# NATEZERO

![natezero.bmp](natezero.bmp)

NATEZERO is a multiplatform networking utility for both retro and modern operating systems to fix the modern internet for retro browsers

Project Goals:

-A proxy service that in a UI looks like shell around a dorky banner ad similar to Netzero or Juno free dial up internet running on all flavors of anything

-Route HTTPS to HTTP and newer versions of HTTP to older versions of HTTP

-Fix images and if possible video to work on retro computers

-Fix modern Javascript/CSS/etc layout or at least break and show content

-Unicode

-Maybe later - see what's up with this project and Lynx

The first step of this being possible by JWZ @ https://www.jwz.org/blog/2008/03/happy-run-some-old-web-browsers-day/
